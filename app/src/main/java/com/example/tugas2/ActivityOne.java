package com.example.tugas2;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityOne extends AppCompatActivity {
    private String nama, nim;
    private TextView textNama, textNim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        Bundle extras = getIntent().getExtras();
        nama = extras.getString("Nama");
        nim = extras.getString("NIM");

        textNama = findViewById(R.id.text_name);
        textNim = findViewById(R.id.text_nim);

        textNama.setText("Username: " + nama);
        textNim.setText("Password:  " + nim);


    }
}
