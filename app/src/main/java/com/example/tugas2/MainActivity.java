package com.example.tugas2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button login;
    private EditText editTextText;
    private EditText editTextNumberPassword3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.button2);

        // Inisialisasi EditText
        editTextText = findViewById(R.id.editTextText);
        editTextNumberPassword3 = findViewById(R.id.editTextNumberPassword3);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Ambil teks dari EditText
                String nama = editTextText.getText().toString();
                String nim = editTextNumberPassword3.getText().toString();

                // Buat Intent dan kirim data tambahan
                Intent intent = new Intent(MainActivity.this, ActivityOne.class);
                intent.putExtra("Nama", nama);
                intent.putExtra("NIM", nim);
                startActivity(intent);
            }
        });

    }
}